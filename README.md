# PROSVISO - Assignment 1
The assignment's goal is to set up a CI/CD pipeline for a given application , to automate the entire development process using the Gitlab CI/CD infrastructure to implement it.

### Repository GitLab link
This is the link to the repository:
https://gitlab.com/leonardodelisi/2019_assignment1_euronics_manager

### Repository GitLab link

### Team students
- Amrani Hamza, 807386
- Leonardo Antonio De Lisi, 808973

### Application 
The application, written in Python language, consists of recording telephone sales, 
checking if products are selled by registered sellers, if not a new seller is added to the database.

Particularly, the database is composed by three tables:
- rivenditore: contains data information about the sellers;
- cellulare: contains data information about the products to sell;
- vendita: contains data information about the sales;

SQLite is used to store data and it interfaces with the application through "sqlite3" library.<br>
The application is implemented with Flask, a lightweight WSGI web application framework, and it is hosted on Heroku. <br> <br> 
[Click here](https://assignment1-euronics-manager.herokuapp.com/) to see the app.

## DevOps
- Containerization: [Docker](https://www.docker.com)
- CI/CD: [GitLab](https://gitlab.com)

### Docker container
The application use one docker image:
- Application: docker image Python 2.7

### GitLab CI/CD pipeline
The GitLab pipeline for the CI / CD is setted in the configuration file ".gitlab-ci.yml".
Whenever there is a new commit the pipeline is performed.

<img src="https://gitlab.com/leonardodelisi/2019_assignment1_euronics_manager/raw/master/gitlab-docker-cicd.jpg" alt="CI/CD Pipeline"/>

The pipeline is divided into 5 stages:

- **Build**: The image building is tested. If the stage is successful, images are pushed to the GitLab registry with the tag: $CONTAINER_IMAGE
    
```
    script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - docker build -t $CONTAINER_IMAGE .
    - docker push $CONTAINER_IMAGE
```

- **Verify**: In this stage we use the library "pyflakes" that works by parsing the source files and checks if there are errors in the code. It’s also much faster.
  
```
    script:
    - cd /app/src/test
    - pyflakes application.py
    - pyflakes creation_db.py
    - pyflakes functions.py
```

- **Test**: The functions application is tested using "unittest" python's library; we developed a parallel stage using four different test files (one for each function) to improve the performance and show that it's possible running multiple scripts together.

```
    script:
    - cd /app/src/test
    - python test_nome.py
    - python test_cognome.py
    - python test_seriale.py
    - python test_modello.py
    
```

- **Release**: The docker image is renamed with the tag "latest", instead of "master". After that it is uploaded to the registry.

```
    script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
    - docker pull $CONTAINER_IMAGE:$CI_COMMIT_REF_NAME || true
    - docker build --cache-from $CONTAINER_IMAGE:$CI_COMMIT_REF_NAME -t $CONTAINER_IMAGE:$CI_COMMIT_REF_NAME .
    - docker push $CONTAINER_IMAGE:$CI_COMMIT_REF_NAME
```

- **Deploy**: the application is uploaded to https://assignment1-euronics-manager.herokuapp.com/. '$HEROKU_SECRET_KEY' contains the api key to connect GitLab to Heroku.
```
    script:
    - dpl --provider=heroku --app=assignment1-euronics-manager --api-key=$HEROKU_SECRET_KEY
```
 







