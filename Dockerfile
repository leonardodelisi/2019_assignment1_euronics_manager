# Use an official Python runtime as a parent image
FROM python:2.7

# Set the working directory to /app
WORKDIR /app

RUN apt-get update

RUN pip install mysql-connector-python
RUN pip install pyflakes
RUN python -m pip install --upgrade setuptools pip wheel

# Copy the current directory contents into the container at /app
COPY . /app

# Run app.py when the container launches
CMD ["python", "test.py"]

