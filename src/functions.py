import sqlite3

def retconn():
	conn = sqlite3.connect('euronics.db')

	return conn


def check_rivenditore(_id):
	conn = retconn()
	results = conn.execute("SELECT _id FROM rivenditore")
	_id = int(_id)

	for row in results:
		if row[0] == _id:
			return True
	return False

def check_id(_id):
	if _id.isdigit():
		return True
	else:
		return False

def check_nome(nome):
	for i in range (0, len(nome)):
		if not (nome[i] >= 'a' and nome[i] <= 'z') and not (nome[i] >= 'A' and nome[i] <= 'Z') and not nome[i] == ' ':
			return False
	return True

def check_cognome(cognome):
	for i in range (0, len(cognome)):
		if not (cognome[i] >= 'a' and cognome[i] <= 'z') and not (cognome[i] >= 'A' and cognome[i] <= 'Z'):
			return False
	return True

def check_n_seriale(n_seriale):
	if(not n_seriale == ''):
		return True
	else:
		return False

def check_seriale_esistente(n_seriale):
	conn= retconn()
	results = conn.execute("SELECT n_seriale FROM cellulare")

	b = False
	for row in results:
		if row[0] == n_seriale:
			b = True
	return b

def check_modello(modello):
	if (not modello == ''):
		return True
	else:
  		return False


def add_rivenditore(_id, nome, cognome):
	conn= retconn()
	try:
		conn.execute("INSERT INTO rivenditore (_id, nome, cognome) VALUES ('%s', '%s', '%s');" %(_id, nome, cognome))
		conn.commit()
	except:
		conn.close()
		return False
	conn.close()
	return True

def add_cellulare(n_seriale,modello):
	conn = retconn()
	try:
		conn.execute("INSERT INTO cellulare(n_seriale,modello) VALUES ('%s', '%s');" %(n_seriale,modello))
		conn.commit()
	except:
		conn.close()
		return False
	conn.close()
	return True

def add_vendita(n_seriale,id_rivenditore):
	conn = retconn()
	results = conn.execute("SELECT n_vendita FROM vendita ORDER BY n_vendita DESC LIMIT 1")

	n_vendita = 1
	for row in results:
		flag=True;
		while(flag):
			n_vendita = row[0] + 1;
			flag= False;

	try:
		conn.execute("INSERT INTO vendita(n_vendita,n_seriale,id_rivenditore) VALUES ('%s', '%s', '%s');" %(n_vendita,n_seriale,id_rivenditore))
		conn.commit()
	except:
		conn.rollback()
		conn.close()
		return False

	conn.close()
	return True

def get_tables(conn):
	rivenditore = conn.execute("SELECT * FROM rivenditore").fetchall()
	cellulare = conn.execute("SELECT * FROM cellulare").fetchall()
	vendita = conn.execute("SELECT * FROM vendita").fetchall()

	return rivenditore, cellulare, vendita
