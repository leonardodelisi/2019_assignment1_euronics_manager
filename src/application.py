#Main application that use creation_db for creating a database 
#and "functions" for checking the input included and adding theese in 
#the database that we've just created
 
import creation_db
import functions

# creation of the database, if not exist
creation_db.create_db()


#the program takes the id input and check in the database if already exist.
#If non exist, creates a new "rivenditore" in the database
#asking the name and the surname 

#All the input by command line are checked by functions 
#that guarantee, for example, that a name doesn't have number inside the string

id_rivenditore = raw_input('Inserire id rivenditore: ')
if(not functions.check_rivenditore(id_rivenditore)):
	print ("\nERRORE id rivenditore non presente nel data base, inizio registrazione...")
	flag=True;
	while(flag):
		id_rivenditore = raw_input('Inserisci nuovamente id rivenditore: ')
		if(not functions.check_id(id_rivenditore)):
			print ("\nERRORE id non valido")
		else:
			flag=False;

	while(not flag):
		nome = raw_input("\nScrivere il nome del rivenditore: ")
		if(not functions.check_nome(nome)):
			print ("\nERRORE nome non valido")
		else:
			flag=True;

	while(flag):
		cognome = raw_input("\nScrivere il cognome del rivenditore: ")
		if(not functions.check_cognome(cognome)):
			print ("\nERRORE cognome non valido")
		else:
			flag=False;
			functions.add_rivenditore(id_rivenditore, nome, cognome)



#Like the rivenditore's input, this part of the code takes the
#smartphone's serial number and check if already exist in the database.
#if not, ask the model and create a new record in the cellulare's table

flag2=True
while(flag2):
	n_seriale = raw_input("Inserire il numero seriale del cellulare: ")
	if(not functions.check_n_seriale(n_seriale)):
		print ("\nERRORE numero seriale non valido")
	else:
		flag2=False;
if(not functions.check_seriale_esistente(n_seriale)):
	print ("\nATTENZIONE numero seriale non ancora registrato.")
	while(not flag2):
		modello = raw_input("Inserire modello cellulare:  ")
		if(not functions.check_modello(modello)):
			print ("\nERRORE modello non valido")
		else:
			flag2=True;
	functions.add_cellulare(n_seriale,modello)


#At the end, to complete the sale, is added a new record 
#in the vendita's table with the information taken previously
print ("\n Vendita registrata!")
functions.add_vendita(n_seriale,id_rivenditore)
