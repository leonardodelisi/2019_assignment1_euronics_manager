import sys
import os,sys,inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(current_dir))
import functions
import unittest

class MyTest(unittest.TestCase):
    def test_check_modello(self):
		self.assertTrue(functions.check_modello('galaxy_s'),msg=None)
		self.assertTrue(functions.check_modello('iphone 8'),msg=None)
		self.assertFalse(functions.check_modello(''),msg=None)

if __name__ == '__main__':
    unittest.main()
