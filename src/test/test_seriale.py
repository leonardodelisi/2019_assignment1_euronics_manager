import sys
import os,sys,inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(current_dir))
import functions
import unittest

class MyTest(unittest.TestCase):
	def test_check_n_seriale(self):
		self.assertTrue(functions.check_n_seriale('02854'),msg=None)
		self.assertTrue(functions.check_n_seriale('SE2648GD'),msg=None)
		self.assertFalse(functions.check_n_seriale(''),msg=None)

if __name__ == '__main__':
    unittest.main()
