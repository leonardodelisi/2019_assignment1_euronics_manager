import sys
import os,sys,inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(current_dir))
import functions
import unittest

class MyTest(unittest.TestCase):
    def test_check_id(self):

        self.assertTrue(functions.check_id('01249'),msg=None)
        self.assertTrue(functions.check_id('3'),msg=None)
        self.assertFalse(functions.check_id('furut k'),msg=None)
        self.assertFalse(functions.check_id('leo$'),msg=None)

    def test_check_nome(self):
        self.assertTrue(functions.check_nome('hamza'),msg=None)
        self.assertTrue(functions.check_nome('LUIGGI'),msg=None)
        self.assertTrue(functions.check_nome('Leonardo Antonio'),msg=None)
        self.assertFalse(functions.check_nome('Pirex 06'),msg=None)
        self.assertFalse(functions.check_nome('M4rc0'),msg=None)

    def test_check_cognome(self):
        self.assertTrue(functions.check_cognome('rossi'),msg=None)
        self.assertTrue(functions.check_cognome('DELISI'),msg=None)
        self.assertTrue(functions.check_cognome('VincenZIno'),msg=None)
        self.assertFalse(functions.check_cognome('.,;'),msg=None)
        self.assertFalse(functions.check_cognome('zio-carlo-magno'),msg=None)

    def test_check_n_seriale(self):
        self.assertTrue(functions.check_n_seriale('02854'),msg=None)
        self.assertTrue(functions.check_n_seriale('SE2648GD'),msg=None)
        self.assertFalse(functions.check_n_seriale(''),msg=None)


    def test_check_modello(self):
        self.assertTrue(functions.check_modello('galaxy_s'),msg=None)
        self.assertTrue(functions.check_modello('iphone 8'),msg=None)
        self.assertFalse(functions.check_modello(''),msg=None)


if __name__ == '__main__':
    unittest.main()
