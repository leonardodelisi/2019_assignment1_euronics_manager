import sys
import os,sys,inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(current_dir)) 
import functions
import unittest

class MyTest(unittest.TestCase):
    def test_check_cognome(self):
        self.assertTrue(functions.check_cognome('rossi'),msg=None)
        self.assertTrue(functions.check_cognome('DELISI'),msg=None)
        self.assertTrue(functions.check_cognome('VincenZIno'),msg=None)
        self.assertFalse(functions.check_cognome('.,;'),msg=None)
        self.assertFalse(functions.check_cognome('zio-carlo-magno'),msg=None)

if __name__ == '__main__':
    unittest.main()
