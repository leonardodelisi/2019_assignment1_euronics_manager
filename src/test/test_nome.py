import sys
import os,sys,inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(current_dir))
import functions
import unittest

class MyTest(unittest.TestCase):
	def test_check_nome(self):
		self.assertTrue(functions.check_nome('hamza'),msg=None)
		self.assertTrue(functions.check_nome('LUIGGI'),msg=None)
		self.assertTrue(functions.check_nome('Leonardo Antonio'),msg=None)
		self.assertFalse(functions.check_nome('Pirex 06'),msg=None)
		self.assertFalse(functions.check_nome('M4rc0'),msg=None)

if __name__ == '__main__':
    unittest.main()
