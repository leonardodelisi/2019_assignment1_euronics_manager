import sqlite3

def create_db():
	conn = sqlite3.connect('euronics.db')
	
	#if tables doesn't exists, create and open it
	conn.execute("CREATE TABLE IF NOT EXISTS rivenditore(_id INT(255) primary key,nome VARCHAR(255), cognome VARCHAR(255));")
	conn.execute("CREATE TABLE IF NOT EXISTS cellulare(n_seriale VARCHAR(255) primary key, modello VARCHAR(255));")
	conn.execute("CREATE TABLE IF NOT EXISTS vendita(n_vendita INT primary key, n_seriale  VARCHAR(255),id_rivenditore INT(255), FOREIGN KEY (n_seriale) REFERENCES cellulare(n_seriale), FOREIGN KEY (id_rivenditore) REFERENCES rivenditore(_id));")
	
	#fill tables "rivenditore and cellulare" with three rows each
	try:
		conn.execute("INSERT INTO rivenditore (_id, nome, cognome) VALUES ('001','Paolo','Fox')")
		conn.execute("INSERT INTO rivenditore (_id, nome, cognome) VALUES ('002','Luigi','Fabrizi')")
		conn.execute("INSERT INTO rivenditore (_id, nome, cognome) VALUES ('003','Carlo','Rossi')")

		conn.commit()
	except:
		conn.rollback()
	try:
		conn.execute("INSERT INTO cellulare(n_seriale, modello) VALUES ('AAA','Iphone_7')")
		conn.execute("INSERT INTO cellulare(n_seriale, modello) VALUES ('AAB','Iphone_XS')")
		conn.execute("INSERT INTO cellulare(n_seriale, modello) VALUES ('AAC','Galaxy_s10')")

		conn.commit()
	except:
		conn.rollback()
	try:
		conn.execute("INSERT INTO vendita(n_vendita,n_seriale,id_rivenditore) VALUES ('001','AAA','001')")
		conn.execute("INSERT INTO vendita(n_vendita,n_seriale,id_rivenditore) VALUES ('002','AAB','003')")

		conn.commit()
	except:
		conn.rollback()
			
	conn.commit()
	conn.close()