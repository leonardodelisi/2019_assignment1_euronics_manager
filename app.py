from flask import Flask, render_template, request
import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, 'src')
import creation_db
import functions


app = Flask(__name__)
creation_db.create_db()

@app.route("/")
def index():
    conn = functions.retconn()
    rivenditore, cellulare, vendita = functions.get_tables(conn)
    conn.close()

    return render_template(
        "index.html", main=True,
        rivenditore = rivenditore,
        cellulare = cellulare,
        vendita = vendita
    )

@app.route('/addRivenditore', methods=['GET', 'POST'])
def addRivenditore():
    id = request.form['idR']
    nome = request.form['nome']
    cognome = request.form['cognome']

    result = functions.add_rivenditore(id,nome,cognome)

    conn = functions.retconn()
    rivenditore, cellulare, vendita = functions.get_tables(conn)
    conn.close()

    return render_template(
        "index.html", main=True,
        rivenditore = rivenditore,
        cellulare = cellulare,
        vendita = vendita
    )

@app.route('/addProdotto', methods=['GET', 'POST'])
def addProdotto():
    seriale = request.form['seriale']
    modello = request.form['modello']

    functions.add_cellulare(seriale,modello)

    conn = functions.retconn()
    rivenditore, cellulare, vendita = functions.get_tables(conn)
    conn.close()

    return render_template(
        "index.html", main=True,
        rivenditore = rivenditore,
        cellulare = cellulare,
        vendita = vendita
    )

@app.route('/addVendita', methods=['GET', 'POST'])
def addVendita():
    serialeV = request.form['serialeV']
    id = request.form['id']

    functions.add_vendita(serialeV,id)

    conn = functions.retconn()
    rivenditore, cellulare, vendita = functions.get_tables(conn)
    conn.close()

    return render_template(
        "index.html", main=True,
        rivenditore = rivenditore,
        cellulare = cellulare,
        vendita = vendita
    )

if __name__ == "__main__":
    app.run()
